package com.example.practica0392;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;

public class MainActivity extends AppCompatActivity {
    private Button btnIngresar;
    private Button btnSalir;
    private EditText txtUsuario;
    private EditText txtContraseña;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();

        btnIngresar.setOnClickListener(v -> ingresar());
        btnSalir.setOnClickListener(v -> salir());
    }

    private void iniciarComponentes() {
        btnIngresar = findViewById(R.id.btnIngresar);
        btnSalir = findViewById(R.id.btnSalir);
        txtUsuario = findViewById(R.id.txtUsuario);
        txtContraseña = findViewById(R.id.txtContraseña);
    }

    private void ingresar() {
        String strUsuario;
        String strContraseña;

        strUsuario = getResources().getString(R.string.usuario);
        strContraseña = getResources().getString(R.string.contraseña);

        if (strUsuario.equals(txtUsuario.getText().toString()) && strContraseña.equals(txtContraseña.getText().toString())) {
            Bundle bundle = new Bundle();
            bundle.putString("usuario", txtUsuario.getText().toString());

            Intent intent = new Intent(MainActivity.this, CalculadoraActivity.class);
            intent.putExtras(bundle);

            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), "El usuario o contraseña no son válidos", Toast.LENGTH_SHORT).show();
        }
    }

    private void salir() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("CALCULADORA");
        confirmar.setMessage("¿Desea cerrar la app?");

        confirmar.setPositiveButton("Confirmar", (dialog, which) -> finish());
        confirmar.setNegativeButton("Cancelar", (dialog, which) -> {});

        confirmar.show();
    }
}
