package com.example.practica0392;


import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class CalculadoraActivity extends AppCompatActivity {

    private Button btnSumar;
    private Button btnRestar;
    private Button btnMulti;
    private Button btnDiv;
    private Button btnLimpiar;
    private Button btnRegresar;
    private TextView lblUsuario;
    private TextView lblResultado;
    private EditText txtUno;
    private EditText txtDos;

    private Calculadora calculadora = new Calculadora(0, 0);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);
        iniciarComponentes();

        btnSumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSumar();
            }
        });

        btnRestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnRestar();
            }
        });

        btnMulti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnMultiplicacion();
            }
        });

        btnDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnDividir();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnLimpiar();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnRegresar();
            }
        });

        Bundle datos = getIntent().getExtras();
        String usuario = datos.getString("usuario");
        lblUsuario.setText(usuario);

    }

    private void iniciarComponentes() {
        btnSumar = findViewById(R.id.btnSumar);
        btnRestar = findViewById(R.id.btnRestar);
        btnMulti = findViewById(R.id.btnMultiplicar);
        btnDiv = findViewById(R.id.btnDividir);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        lblUsuario = findViewById(R.id.lblUsuario);
        lblResultado = findViewById(R.id.lblResultado);
        txtUno = findViewById(R.id.txtNum1);
        txtDos = findViewById(R.id.txtNum2);

    }

    private void btnSumar() {
        if (txtUno.getText().toString().equals("") || txtDos.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Complete los campos", Toast.LENGTH_SHORT).show();
        } else {
            calculadora.setNum1(Integer.parseInt(txtDos.getText().toString()));
            calculadora.setNum2(Integer.parseInt(txtDos.getText().toString()));
            int total = calculadora.suma();
            lblResultado.setText(""+total);
        }
    }

    private void btnRestar() {
        if (txtUno.getText().toString().equals("") || txtDos.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Complete los campos", Toast.LENGTH_SHORT).show();
        } else {
            calculadora.setNum1(Integer.parseInt(txtUno.getText().toString()));
            calculadora.setNum2(Integer.parseInt(txtDos.getText().toString()));
            int total = calculadora.resta();
            lblResultado.setText(""+total);
        }
    }

    private void btnMultiplicacion() {
        if (txtUno.getText().toString().equals("") || txtDos.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Complete los campos", Toast.LENGTH_SHORT).show();
        } else {
            calculadora.setNum1(Integer.parseInt(txtUno.getText().toString()));
            calculadora.setNum2(Integer.parseInt(txtDos.getText().toString()));
            int total = calculadora.multiplicacion();
            lblResultado.setText(""+total);
        }
    }

    private void btnDividir() {
        if (txtUno.getText().toString().equals("") || txtDos.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Complete los campos", Toast.LENGTH_SHORT).show();
        } else {
            calculadora.setNum1(Integer.parseInt(txtUno.getText().toString()));
            calculadora.setNum2(Integer.parseInt(txtDos.getText().toString()));
            int total = calculadora.division();
            lblResultado.setText(""+total);
        }
    }

    private void btnLimpiar() {
        lblResultado.setText("");
        txtUno.setText("");
        txtDos.setText("");
    }

    private void btnRegresar() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Calculadora");
        confirmar.setMessage("¿Desea regresar al MainActivity?");

        confirmar.setPositiveButton("Confirmar", (dialog, which) -> finish());
        confirmar.setNegativeButton("Cancelar", (dialog, which) -> {});

        confirmar.show();
    }
}

